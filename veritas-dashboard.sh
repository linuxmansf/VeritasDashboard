#############################################################################
#									    #
#!/bin/bash								    #
# Veritas Monitoring Dashboard v1.0					    #
# Main purpose of this script is monitoring Veritas product with menu 	    #
# driven under cli							    #
#									    #
# Written by Egemen Bal 2016						    #
# 									    #
#############################################################################

clear

################
### Coloring ###
################
REDOUT='\033[0;41;30m'
RED='\E[33;31m'
BLUEOUT='\E[37;44m'
YELLOW='\E[33;33m'
WHITEOUT='\E[30;47m'
STD='\033[0;0;39m'

#####################################
### trap ctrl-c and call ctrl_c() ###
#####################################
trap ctrl_c INT

################
### Check OS ###
################
#OS=$(lsb_release -si)
OS=$(uname -a)
AR=$(uname -i)
ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')
VER=$(lsb_release -sr)

#################
### FUNCTIONS ###
#################

# press any key or abort function
function pressanykeyorabort()
{
	echo
		echo
		read -n1 -r -p $'\E[37;44mPress any key to continue or "a" to abort...\e[0m' key
		clear
		if [[ $key == a ]] ; then
			break
				fi
}


# press any key function
function pressanykey()
{
	echo
		echo
		read -n1 -r -p $'\E[37;44mPress any key to continue...\e[0m' key
		clear
}

#################
### MAIN MENU ###
#################

function main_menu()
{

function ctrl_c() {
        clear
        main_menu
}

while :
	do
		echo
			echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
			echo -e "${WHITEOUT}           MAIN MENU              ${STD}"
			echo
			echo "1. Licensing"
			echo "2. Monitor"
			echo "3. Performance"
			echo "4. Cluster"
			echo "5. VVR"
			echo "6. Logs"
			echo
			echo
			echo "q. Quit"
			echo
			read -p $'\E[37;44mChoice: \e[0m ' m_choice

			case "$m_choice" in
			1) clear
			sub_menu_licensing
			;;
	2) clear
		sub_menu_monitor
		;;
	3) clear
		sub_menu_performance
		;;
	4) clear
		sub_menu_cluster
		;;
	5) clear
		sub_menu_vvr
		;;
	6) clear
		sub_menu_logs
		;;
	q) clear
		exit 0
		;;
	*)	echo 
		printf "Invalid Option"
		sleep 0.45
		clear
		main_menu
		;;
	esac
		done
}
# 


function pause(){
	read -p "$*"
}

##################################### SUB MENUS ###########################################

############################
### sub menu - Licensing ###
############################

function sub_menu_licensing()
{

function ctrl_c() {
        clear
        sub_menu_licensing
}

while :
	do
		echo
			echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
			echo -e "${WHITEOUT}      » 1. Licensing              ${STD}"
			printf "\n"
			echo "1. Family Product Name"
			echo "2. Installed packages"
			echo "3. License Informations"
			echo "4. Detailed License List"
			echo
			echo
			echo "b. Back to Main Menu"
			echo "q. Quit"
			printf "\n"
			read  -p $'\E[37;44mChoice: \e[0m '  s_choice
			echo

			case "$s_choice" in
			1)  clear
			printf "\n\n"
			echo -e "${YELLOW}# vxkeyless -v display ${STD}\n"
			vxkeyless -v display
			pressanykey
			;;
	2)  clear
		printf "\n\n"
##
		if [[ $OS == *"Linux"* ]]
			then
				c1=$(rpm -qa | grep VRTS)
				c2="yum list | grep VRTS"
		else
			c1=$(pkg list | grep VRTS)
				c2="pkg list | grep VRTS"
				fi
##
				echo -e "${YELLOW}# $c2 ${STD}\n"
				printf "\n$c1"
				pressanykey
				;;
	3)  clear
		printf "\n\n"
		echo -e "${YELLOW}# vxkeyless displayall ${STD}\n"
		vxkeyless displayall | more
		pressanykey
		;;
	4)  clear
		printf "\n\n"
		echo -e "${YELLOW}# vxlicrep  ${STD}\n"
		vxlicrep | more
		pressanykey
		;;
	b)  clear
		main_menu
		;;
	q) clear
		exit 0
		;;
	*)  echo
		printf "Invalid Option"
		sleep 0.45
		clear
		;;
	esac
		done
}



#################################
### sub menu - Monitor        ###
#################################

function sub_menu_monitor()
{

function ctrl_c() {
        clear
        sub_menu_monitor
}

while :
	do
		echo
			echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
			echo -e "${WHITEOUT}      » 2. Monitor                ${STD}"
			printf "\n"
			printf "\n"
			echo "1. List disk groups"
			echo "2. List Volumes"
			echo "3. Display disk listings"
			echo "4. Detail Disk List"
			echo "5. Display volume manager object listings"
			echo "6. Display free space in a disk group"
			echo "7. Display multipath information"
			echo "8. Display disk group information"
			echo "9. Display mount options"
			echo "10. List background tasks"
			echo "11. List SAN"
			echo "12. List DMP"

			echo
			echo
			echo "b. Back to Main Menu"
			echo "q. Quit"
			printf "\n"
			read  -p $'\E[37;44mChoice: \e[0m '  m_choice
			echo
			case "$m_choice" in
			1)  clear
			printf "\n\n"
			echo -e "${YELLOW}# vxlist dg ${STD}"
			printf "\n\n"
			vxlist dg | more
			pressanykey
			;;
	2) clear
		printf "\n\n"
		echo -e "${YELLOW} # vxlist vol ${STD}"
		printf "\n\n"
		vxlist vol | more
		pressanykey
		;;
	3) clear
		vxlist dg | grep enabled | awk '{print $2}' | grep -v DISKGROUP > /tmp/vrtsmonit1.tmp
		for a in `cat /tmp/vrtsmonit1.tmp`
			do
				printf "\n\n"
					echo -e "${YELLOW} # vxdg list $a ${STD}"
					printf "\n\n"
					vxdg list $a | more
					pressanykeyorabort
					done
					;;
			4) clear
				printf "\n\n"
				echo -e "${YELLOW} # vxdisk -eo alldgs list  ${STD}"
				printf "\n\n"
				vxdisk -eo alldgs list | more
				pressanykey
				;;
			5) clear
				printf "\n\n"
				echo -e "${YELLOW} # vxprint -htuh  ${STD}"
				printf "\n\n"
				vxprint -htuh | more
				pressanykey
				;;
			6) clear
				vxlist dg | grep enabled | awk '{print $2}' | grep -v DISKGROUP > /tmp/vrtsmonit1.tmp
				for a in `cat /tmp/vrtsmonit1.tmp`
					do
						printf "\n\n"
							echo -e "${YELLOW} # vxdg -g $a free ${STD}"
							printf "\n\n"
							vxdg -g $a free
							pressanykeyorabort
							done
							;;
					7) clear
						vxlist disk | awk '{print $2}' | grep -v DEVICE |grep -v sd | tr '\n' ' ' > /tmp/vrtsmonit1.tmp
						LIST="$(cat /tmp/vrtsmonit1.tmp)"
						printf "\n\n"
						echo -e "${YELLOW} # vxdisk list $LIST | grep -A1 'Device\|numpaths' ${STD}"
						printf "\n\n"
						vxdisk list $LIST | grep -A1 'Device\|numpaths' | more
						pressanykeyorabort
						;;
					8) clear
						vxlist dg | grep enabled | awk '{print $2}' | grep -v DISKGROUP > /tmp/vrtsmonit1.tmp
						for a in `cat /tmp/vrtsmonit1.tmp`
							do
								printf "\n\n"
									echo -e "${YELLOW} # vxdg list $a ${STD}"
									printf "\n\n"
									vxdg list $a
									pressanykeyorabort
									done
									;;
							9) clear
								printf "\n\n"
								echo -e "${YELLOW} # mount | grep vx | grep -v "tmpfs\|none" | awk '{print \$3,\$6}'  ${STD}"
								printf "\n\n"
								mount | grep vx | grep -v "tmpfs\|none" | awk '{print $3,$6}'
								pressanykey
								;;
							10) clear
								printf "\n\n"
								echo -e "${YELLOW} # vxtask list ${STD}"
								printf "\n\n"
								vxtask list
								pressanykey
								;;
							11) clear
								printf "\n\n"
								echo -e "${YELLOW} # vxdmpadm listenclosure ${STD}"
								printf "\n\n"
								vxdmpadm listenclosure | more
								pressanykey
								;;
							12) clear
								printf "\n\n"
								echo -e "${YELLOW} # vxdmpadm getsubpaths ${STD}"
								printf "\n\n"
								vxdmpadm getsubpaths | more
								pressanykey
								;;
							b)  clear
								main_menu
								;;
							q) clear
								exit 0
								;;
							*)  echo "Invalid Option"
								read -n1 -r -p "Press any key to continue..." key
								clear
								;;
							esac
								done
}


###################################
### sub menu - Performance      ###
###################################

function sub_menu_performance()
{

function ctrl_c() {
        clear
        sub_menu_performance
}

while :
	do
		echo
			echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
			echo -e "${WHITEOUT}      » 3. Performance            ${STD}"
			printf "\n"
			echo "1. Disk I/O Performance"
			echo "2. VXFS counters for volume"
			echo "3. VXSTAT statics"
			echo "4. vxdmp statics"
			echo "5. Odm statics"
			echo
			echo
			echo "b. Back to Main Menu"
			echo "q. Quit"
			printf "\n"
			read  -p $'\E[37;44mChoice: \e[0m '  s_choice
			echo

			case "$s_choice" in
			1)  clear
			printf "\n\n"
			echo -e "${YELLOW}# iostat -xtnc 3 360 | grep "AM\|PM\|avg-cpu\|Device\|VxVM" ${STD}\n"
			echo "CTRL-C to break"
			echo
        if [[ $OS == *"SunOS"* ]]; then
        iostat -xtnceT d 3 360 | grep "AM\|PM\|avg-cpu\|Device\|VxVM"
        fi

        if [[ $OS == *"Linux"* ]]; then
                        iostat -xtnc 3 360 | grep "AM\|PM\|avg-cpu\|Device\|VxVM"
        fi

			pressanykey
			;;

	2) clear
		df -Pk | grep vx | grep -v tmpfs | awk '{print $6}' > /tmp/vrtsmonit1.tmp
		mapfile -t addrlist <  /tmp/vrtsmonit1.tmp
		printf "Which mount point do you want to run for?\n"
		select addr in "${addrlist[@]// /-}"; do
		clear
		k=1
	while [ $k -lt 6 ]; do
		clear
			echo -e "${YELLOW} # /opt/VRTS/bin/vxfsstat -c 5 -t 3 $addr ${STD}"
			echo
			echo -e "${RED} Count: #$k/5 ${STD} Interval 3 seconds"
			printf "\n\n"
			echo "Press a to abort"
			printf "\n\n"
			/opt/VRTS/bin/vxfsstat "$addr"
			read -t 1 -n 1 key
			if [[ $key = a ]]; then
				break
					fi
					sleep 3
					k=`expr $k + 1`
					done
					pressanykey
					break
					done
					sub_menu_performance
					;;

	3) clear
		vxlist dg | grep enabled | awk '{print $2}' | grep -v DISKGROUP > /tmp/vrtsmonit1.tmp
		mapfile -t addrlist <  /tmp/vrtsmonit1.tmp
		printf "Which disk group do you want to run for?\n"
		select addr in "${addrlist[@]// /-}"; do
		clear
		k=1
		while [ $k -lt 6 ]; do
			clear
				echo -e "${YELLOW} # vxstat -g $addr -vps -i 3 -c 5 ${STD}"
				echo
				echo -e "${RED} Count: #$k/5 ${STD} Interval 3 seconds"
				printf "\n\n"
				echo "Press a to abort"
				printf "\n\n"
				vxstat -g "$addr" -vps 
				read -t 1 -n 1 key
				if [[ $key = a ]]; then
					clear
						trap '' INT
						sub_menu_performance
						fi
						sleep 3
						k=`expr $k + 1`
						done
						pressanykey
						break
						done
						sub_menu_performance
						;;
        4) clear
		printf "\n\n"
                k=1
                while [ $k -lt 6 ]; do
		clear
		echo -e "${YELLOW}# vxdmpadm iostat show all interval=3 count=6  ${STD}\n"
		echo
                echo -e "${RED} Count: #$k/5 ${STD} Interval 3 seconds"
		echo
		vxdmpadm iostat show all interval=3 count=1
 		sleep 3
                k=`expr $k + 1`
                done
		pressanykey
               ;;

         5) clear
	printf "\n\n"
               read -r -p "Please type full path of ODM file [seperate multiple files with semi-colon ';']" odmmountpoints
               IFS=';' read -ra odmpaths <<< "$odmmountpoints"
               for i in "${odmpaths[@]}"; do
               odmmountpointsF=${i////-}
	       echo -e "${YELLOW}# /opt/VRTS/bin/odmstat -i 1 -c 360 $i  ${STD}\n"
               /opt/VRTS/bin/odmstat -i 1 -c 360 $i
               done
		clear
	;;

	b)  clear
		main_menu
		;;
	q) clear
		exit 0
		;;
	*)  echo
		printf "Invalid Option"
		sleep 0.45
		clear
		;;
	esac
		done
}


############################
### sub menu - Cluster   ###
############################

function sub_menu_cluster()
{

function ctrl_c() {
        clear
        sub_menu_cluster
}

    while :
    do
        echo
        echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
        echo -e "${WHITEOUT}      » 4. Cluster                ${STD}"
        printf "\n"
        echo "1. Cluster Overview"
        echo "2. Summary"
        echo "3. Detailed Status"
        echo "4. Display Cluster"
        echo "5. Cluster State"
        echo "6. Node State"
        echo "7. Gab Ports"
        echo "8. Fencing Membership"
        echo "9. Display Faults"
        echo "10. List depencies"
        echo "11. LLT Status"
        echo "12. Verify Cluster Configuration"
        echo "13. List Cluster Users"
        echo "14. Display attributes"
        echo "15. Check Policy for CFSMounts"
        echo "16. Check Logs"
        echo
        echo
        echo "b. Back to Main Menu"
        echo "q. Quit"
        printf "\n"
        read  -p $'\E[37;44mChoice: \e[0m '  s_choice
        echo

        case "$s_choice" in
            1)  clear
                printf "\n\n"
                echo -e "${YELLOW}# cfscluster status ${STD}\n"
                cfscluster status
                pressanykey
                ;;

            2)  clear
                printf "\n\n"
                echo -e "${YELLOW}# hastatus -sum ${STD}\n"
                hastatus -sum
                pressanykey
                ;;

            3)  clear
                printf "\n\n"
                echo -e "${YELLOW}# hasys -display ${STD}\n"
                hasys -display | more
                pressanykey
                ;;

            4)  clear
                printf "\n\n"
                echo -e "${YELLOW}# haclus -display  ${STD}\n"
                haclus -display | more
                pressanykey
                ;;

            5)  clear
                printf "\n\n"
                echo -e "${YELLOW}# hasys -state ;  /etc/vx/bin/vxclustadm  -v nidmap ; vxdctl -c mode ; cfsdgadm display -v   ${STD}\n\n"
                hasys -state  
		printf "\n\n"
		/etc/vx/bin/vxclustadm  -v nidmap
		printf "\n\n"
                vxdctl -c mode
		printf "\n\n"
                cfsdgadm display -v
		printf "\n\n"
                pressanykey
                ;;
            6)  clear
                printf "\n\n"
                echo -e "${YELLOW}# /etc/vx/bin/vxclustadm -v nodestate  ${STD}\n"
                /etc/vx/bin/vxclustadm -v nodestate | more
                pressanykey
                ;;

            7)  clear
                printf "\n\n"
                echo -e "${YELLOW}# gabconfig -a   ${STD}\n"
                gabconfig -a 
                pressanykey
                ;;

            8)  clear
                printf "\n\n"
                echo -e "${YELLOW}# vxfenadm -d   ${STD}\n"
                vxfenadm -d | more
                pressanykey
                ;;

            9)  clear
                printf "\n\n"
                echo -e "${YELLOW}# haagent -display |grep Faults   ${STD}\n"
                haagent -display |grep Faults | more
                pressanykey
                ;;
            10)  clear
                printf "\n\n"
                echo -e "${YELLOW}# hares -dep   ${STD}\n"
                hares -dep | more
                pressanykey
                ;;

            11)  clear
                printf "\n\n"
                echo -e "${YELLOW}# lltstat -l ; lltstat ; lltstat -nvv ; cat /etc/llthosts ; cat /etc/llttab  ${STD}\n"
	        cat /etc/llthosts 
                printf "\n\n"
                read -n1 -r -p $'\E[37;44mPress any key to continue...\e[0m' key
if [ "$key" = '' ] ; then
                printf "\n\n"
        	cat /etc/llttab 
fi

                printf "\n\n"
                read -n1 -r -p $'\E[37;44mPress any key to continue...\e[0m' key
if [ "$key" = '' ] ; then
                printf "\n\n"
		lltstat -l  | more
fi

                printf "\n\n"
                read -n1 -r -p $'\E[37;44mPress any key to continue...\e[0m' key
if [ "$key" = '' ] ; then
                printf "\n\n"
	        lltstat  | more
fi

                printf "\n\n"
                read -n1 -r -p $'\E[37;44mPress any key to continue...\e[0m' key
if [ "$key" = '' ] ; then
                printf "\n\n"
	        lltstat -nvv  | more
fi
                pressanykey
                ;;

            12)  clear
                printf "\n\n"
                echo -e "${YELLOW}# hacf -verify /etc/VRTSvcs/conf/config   ${STD}\n"
                hacf -verify /etc/VRTSvcs/conf/config | more
                pressanykey
                ;;

            13)  clear
                printf "\n\n"
                echo -e "${YELLOW}# hauser -display  ${STD}\n"
                hauser -display | more
                pressanykey
                ;;

            14)  clear
                printf "\n\n"
                read -r -p "Do you want to specify display name ? [example: Application]" response
                read -r -p "Do you want to specify attribute ? [example: MonitorTimeout,MonitorInterval] " response2
                echo -e "${YELLOW}# hatype -display $response -attribute $response2 ${STD}\n"
                hatype -display $response -attribute $response2| more
                pressanykey
                ;;

             15) clear
                df -Pk | grep vx | grep -v tmpfs | awk '{print $6}' > /tmp/vrtsmonit1.tmp
                mapfile -t addrlist <  /tmp/vrtsmonit1.tmp
                printf "Which mount point do you want to run for?\n"
                select addr in "${addrlist[@]// /-}"; do
		clear
                        echo -e "${YELLOW} # fsclustadm -v getpolicy $addr ; fsclustadm -v showprimary $addr ${STD}"
                        printf "\n\n"
			echo -e "${RED}Getpolicy ${STD}"
                        fsclustadm -v getpolicy "$addr" 
			echo
			echo -e "${RED}ShowPrimary${STD}"
			fsclustadm -v showprimary "$addr"
		break
		done
                pressanykey
                ;;


            16) clear
                printf "\n\n"

    while :
    do
	clear
        echo
        echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
        echo -e "${WHITEOUT}      » 4. Cluster                ${STD}"
        echo -e "${WHITEOUT}        » 16. Logging             ${STD}"
        printf "\n"
        echo "1. Show Engine_A Log"
        echo "2. Show /var/messages Log"
        echo "3. Show Command Log"
        echo
        echo "b. Back To Cluster Menu"
        echo "q. Quit"
        printf "\n"
        read -p $'\E[37;44mChoice: \e[0m '  s_choice
        echo

        case "$s_choice" in
            1)  clear
		hamsg engine_A |more
		pressanykey
                ;;
            2)  clear
		tail -200 /var/log/messages | more
		pressanykey
        	;;
            3)  clear
		tail -200 /etc/vx/log/cmdlog | more
                pressanykey
                ;;

            b)  clear
                sub_menu_cluster
                ;;
            q) clear
               exit 0
               ;;
            *)  echo
                printf "Invalid Option"
                sleep 0.45
                clear
                ;;
        esac
    done


;;


            b)  clear
                main_menu
                ;;
            q) clear
               exit 0
               ;;
            *)  echo
                printf "Invalid Option"
                sleep 0.45
                clear
                ;;
        esac
    done
}


############################
### sub menu - VVR       ###
############################

function sub_menu_vvr()
{

function ctrl_c() {
        clear
        sub_menu_vvr
}
    while :
    do
        echo
        echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
        echo -e "${WHITEOUT}      » 5. VVR                    ${STD}"
        printf "\n"
        echo "1. VVR Stat"
        echo "2. Rvg status"
        echo "3. Rlink status"
        echo "4. Replication status"
        echo "5. Print Info"
        echo "6. Show settings"
        echo
        echo
        echo "b. Back to Main Menu"
        echo "q. Quit"
        printf "\n"
        read  -p $'\E[37;44mChoice: \e[0m '  s_choice
        echo

#                vvrdiskgroup=$(vxprint -Pl | grep "Disk group" | awk '{print $3}')
#                vvrrlink=$(vxprint -Pl | grep "Rlink" | awk '{print $2}')
#		 vxprint -Pl | grep rvg | head -1 | awk '{print $2}'  | cut -c 5-

        case "$s_choice" in
            1)  clear
                printf "\n\n"
                echo -e "${YELLOW}# vrstat -R ${STD}\n"
		echo "CTRL-C to break"
		vrstat -R
                pressanykey
                ;;

            2)  clear
                printf "\n\n"
                rvgname=$(vxprint -Pl | grep rvg | head -1 | awk '{print $2}'  | cut -c 5-)
                echo -e "${YELLOW}# vxrvg -g vvrdg stats $rvgname ${STD}\n"
                vxrvg -g vvrdg stats $rvgname
                pressanykey
                ;;

            3)  clear
                printf "\n\n"
                rlinkname=$(vxprint -Pl | grep Rlink | awk '{print $2}')
                echo -e "${YELLOW}# vxrlink -g vvrdg -T -i 5 status $rlinkname  ${STD}\n"
		echo "CTRL-C to break"
                vxrlink -g vvrdg -T -i 5 status $rlinkname
                pressanykey
                ;;

            4)  clear
                printf "\n\n"
                rvgname=$(vxprint -Pl | grep rvg | head -1 | awk '{print $2}'  | cut -c 5-)
                echo -e "${YELLOW}# vradmin -g vvrdg repstatus $rvgname ${STD}\n"
		vradmin -g vvrdg repstatus $rvgname
                pressanykey
                ;;

            5)  clear
                printf "\n\n"
                echo -e "${YELLOW}# vxprint -Pl ${STD}\n"
                vxprint -Pl
                pressanykey
                ;;

            6)  clear
                printf "\n\n"
                echo -e "${YELLOW}# vradmin -l printrvg ${STD}\n"
		vradmin -l printrvg
                pressanykey
                ;;


            b)  clear
                main_menu
                ;;
            q) clear
               exit 0
               ;;
            *)  echo
                printf "Invalid Option"
                sleep 0.45
                clear
                ;;
        esac
    done
}


############################
### sub menu - Logs      ###
############################

function sub_menu_logs()
{

function ctrl_c() {
        clear
        sub_menu_logs
}
    while :
    do
        clear
        echo
        echo -e "${WHITEOUT}    VERITAS MONITOR DASHBOARD     ${STD}"
        echo -e "${WHITEOUT}      » 6. Logging                ${STD}"
        printf "\n"
        echo "1. Show Engine_A Log"
        echo "2. Show /var/messages Log"
        echo "3. Show Command Log"
        echo
        echo "b. Back To Main Menu"
        echo "q. Quit"
        printf "\n"
        read -p $'\E[37;44mChoice: \e[0m '  s_choice
        echo

        case "$s_choice" in
            1)  clear
                read -r -p "Which Month (Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec) ? " response
                read -r -p "Which Day (1,2,3.....30,31) ? " response2
                printf "\n\n"
                echo -e "${YELLOW}# hamsg engine_A  ${STD}\n"
                hamsg engine_A | grep "$response2 $response" | more
                pressanykey
                ;;
            2)  clear
                read -r -p "Which Month (Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec) ? " response
                read -r -p "Which Day (1,2,3.....30,31) ? " response2
                printf "\n\n"
                echo -e "${YELLOW}# tail -200 /var/log/messages ${STD}\n"
                tail -200 /var/log/messages | grep "$response2 $response"| more
                pressanykey
                ;;
            3)  clear
                read -r -p "Which Month (Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec) ? " response
                read -r -p "Which Day (1,2,3.....30,31) ? " response2
                printf "\n\n"
                echo -e "${YELLOW}# tail -200 /etc/vx/log/cmdlog ${STD}\n"
                tail -200 /etc/vx/log/cmdlog | grep "$response $response2" | more
                pressanykey
                ;;

            b)  clear
                main_menu
                ;;
            q) clear
               exit 0
               ;;
            *)  echo
                printf "Invalid Option"
                sleep 0.45
                clear
                ;;
        esac
    done
}
##################################### END SUB MENUS ###########################################



# END OF MAIN MENU
main_menu
exit 0
